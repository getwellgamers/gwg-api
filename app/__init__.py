# app/__init__.py

import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt

# instantiate extensions
db = SQLAlchemy()
migrate = Migrate()
bcrypt = Bcrypt()


def create_app():
    # instantiate app
    app = Flask(__name__)

    # enable CORS
    CORS(app)

    # set config
    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    # set extensions
    db.init_app(app)
    migrate.init_app(app, db)
    bcrypt.init_app(app)

    # register blueprints
    from app.api.users import users_blueprint
    from app.api.auth import auth_blueprint
    from app.api.collections import collections_blueprint
    from app.api.donations import donations_blueprint
    from app.api.hospitals import hospitals_blueprint
    from app.api.items import items_blueprint
    from app.api.itemtypes import itemtypes_blueprint
    app.register_blueprint(users_blueprint)
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(collections_blueprint)
    app.register_blueprint(donations_blueprint)
    app.register_blueprint(hospitals_blueprint)
    app.register_blueprint(items_blueprint)
    app.register_blueprint(itemtypes_blueprint)

    return app