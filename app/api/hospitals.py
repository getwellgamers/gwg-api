# app/api/hospitals.py

from flask import Blueprint, jsonify, request
from sqlalchemy import and_
from sqlalchemy import exc

from app.api.models import Hospital
from app import db

hospitals_blueprint = Blueprint('hospitals', __name__)


@hospitals_blueprint.route('/hospitals', methods=['POST'])
def add_hospital():
    post_data = request.get_json()
    if not post_data:
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload'
        }
        return jsonify(response_object), 400
    name = post_data.get('name')
    location = post_data.get('location')
    try:
        hospital = Hospital.query.filter(and_(Hospital.name==name,  Hospital.location==location)).first()
        if not hospital:
            db.session.add(Hospital(name, location))
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': name
            }
            return jsonify(response_object), 201
        else:
            response_object = {
                'status': 'fail',
                'message': 'Hospital already exists'
            }
            return jsonify(response_object), 400

    except (exc.IntegrityError, ValueError) as e:
        db.session.rollback()
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload'
        }
        return jsonify(response_object), 400


@hospitals_blueprint.route('/hospitals', methods=['GET'])
def get_all_hospitals():
    """Get all hospitals"""
    hospitals = Hospital.query.all()
    hospitals_list = []
    for hospital in hospitals:
        hospital_object = {
            'id': hospital.id,
            'name': hospital.name,
            'location': hospital.location
        }
        hospitals_list.append(hospital_object)
    response_object = {
        'status': 'success',
        'data': {
            'hospitals': hospitals_list
        }
    }
    return jsonify(response_object), 200


@hospitals_blueprint.route('/hospitals/<hospital_id>', methods=['GET'])
def get_single_hospital(hospital_id):
    """Get a single hospital"""
    response_object = {
        'status': 'fail',
        'message': 'Hospital does not exist'
    }
    try:
        hospital = Hospital.query.filter_by(id=hospital_id).first()
        if not hospital:
            return jsonify(response_object), 404
        else:
            response_object = {
                'status': 'success',
                'data': {
                    'id': hospital.id,
                    'name': hospital.name,
                    'location': hospital.location
                }
            }
            return jsonify(response_object), 200
    except ValueError:
        return jsonify(response_object), 404