# app/api/items.py

from flask import Blueprint, jsonify, request

from app.api.models import Item
from app import db

items_blueprint = Blueprint('items', __name__)