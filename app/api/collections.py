# app/api/collections.py

from flask import Blueprint, jsonify, request

from app.api.models import Collection
from app import db

collections_blueprint = Blueprint('collections', __name__)