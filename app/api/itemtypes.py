# app/api/itemtypes.py

from flask import Blueprint, jsonify, request
from sqlalchemy import exc

from app.api.models import ItemType
from app import db

itemtypes_blueprint = Blueprint('itemtypes', __name__)


@itemtypes_blueprint.route('/itemtypes', methods=['POST'])
def add_itemtype():
    post_data = request.get_json()
    if not post_data:
        response_object = {
            'message': 'Invalid payload',
            'status': 'fail'
        }
        return jsonify(response_object), 400
    name = post_data.get('name')
    try:
        itemtype = ItemType.query.filter_by(name=name).first()
        if not itemtype:
            db.session.add(ItemType(name))
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': name
            }
            return jsonify(response_object), 201
        else:
            response_object = {
                'status': 'fail',
                'message': 'Itemtype already exists'
            }
            return jsonify(response_object), 400
    except (exc.IntegrityError, ValueError) as e:
        db.session.rollback()
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload'
        }
        return jsonify(response_object), 400


@itemtypes_blueprint.route('/itemtypes', methods=['GET'])
def get_all_itemtypes():
    """Get all itemtypes"""
    itemtypes = ItemType.query.all()
    itemtypes_list = []
    for itemtype in itemtypes:
        itemtype_object = {
            'id': itemtype.id,
            'name': itemtype.name
        }
        itemtypes_list.append(itemtype_object)
    response_object = {
        'status': 'success',
        'data': {
            'itemtypes': itemtypes_list
        }
    }
    return jsonify(response_object), 200


@itemtypes_blueprint.route('/itemtypes/<itemtype_id>', methods=['GET'])
def get_single_itemtype(itemtype_id):
    """Get a single itemtype"""
    response_object = {
        'status': 'fail',
        'message': 'Itemtype does not exist'
    }
    try:
        itemtype = ItemType.query.filter_by(id=itemtype_id).first()
        if not itemtype:
            return jsonify(response_object), 404
        else:
            response_object = {
                'status': 'success',
                'data': {
                    'id': itemtype.id,
                    'name': itemtype.name
                }
            }
            return jsonify(response_object), 200
    except ValueError:
        return jsonify(response_object), 404


