# app/api/donations.py

import datetime
from dateutil.parser import parse
from flask import Blueprint, jsonify, request
from sqlalchemy import exc

from app.api.models import Donation
from app import db

donations_blueprint = Blueprint('donations', __name__)


@donations_blueprint.route('/donations', methods=['POST'])
def add_donation():
    post_data = request.get_json()
    if not post_data:
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload'
        }
        return jsonify(response_object), 400
    if post_data.get('date'):
        date = parse(post_data.get('date'), dayfirst=True)
    else:
        date = None
    hospital_id = post_data.get('hospital_id')
    user_id = post_data.get('user_id')
    try:
        db.session.add(Donation(date, hospital_id, user_id))
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'Donation added'
        }
        return jsonify(response_object), 201
    except (exc.IntegrityError, ValueError) as e:
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload'
        }
        return jsonify(response_object), 400


@donations_blueprint.route('/donations', methods=['GET'])
def get_all_donations():
    """Get all donations"""
    donations = Donation.query.all()
    donations_list = []
    for donation in donations:
        donation_object = {
            'id': donation.id,
            'date': str(donation.date),
            'hospital_id': donation.hospital_id,
            'user_id': donation.user_id
        }
        donations_list.append(donation_object)
    response_object = {
        'status': 'success',
        'data': {
            'donations': donations_list
        }
    }
    return jsonify(response_object), 200


@donations_blueprint.route('/donations/<donation_id>', methods=['GET'])
def get_single_donation(donation_id):
    """Get a single donation"""
    response_object = {
        'status': 'fail',
        'message': 'Donation does not exist'
    }
    try:
        donation = Donation.query.filter_by(id=donation_id).first()
        if not donation:
            return jsonify(response_object), 404
        else:
            response_object = {
                'status': 'success',
                'data': {
                    'id': donation.id,
                    'date': str(donation.date),
                    'hospital_id': donation.hospital_id,
                    'user_id': donation.user_id
                }
            }
            return jsonify(response_object), 200

    except ValueError:
        return jsonify(response_object), 404