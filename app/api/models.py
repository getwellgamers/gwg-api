# app/api/models.py

import datetime

import jwt
from flask import current_app
from app import db, bcrypt


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(128), nullable=False, unique=True)
    email = db.Column(db.String(128), nullable=False, unique=True)
    password = db.Column(db.String(256), nullable=False)
    location = db.Column(db.String(128), nullable=False)
    collections = db.relationship('Collection', backref='user')
    items = db.relationship('Item', backref='user')
    donations = db.relationship('Donation', backref='user')

    def __init__(self, username, email, password, location):
        self.username = username
        self.email = email
        self.password = bcrypt.generate_password_hash(password, current_app.config.get('BCRYPT_LOG_ROUNDS')).decode()
        self.location = location

    def encode_auth_token(self, user_id):
        """Generates an auth token"""
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(
                    days=current_app.config.get('TOKEN_EXPIRATION_DAYS'),
                    seconds=current_app.config.get('TOKEN_EXPIRATION_SECONDS')
                ),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(payload, current_app.config.get('SECRET_KEY'), algorithm='HS256')
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """Decodes an auth token"""
        try:
            payload = jwt.decode(auth_token, current_app.config.get('SECRET_KEY'))
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in.'


class Collection(db.Model):
    __tablename__='collections'
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    postcode = db.Column(db.String(32), nullable=False)
    lng = db.Column(db.Float, nullable=False)
    lat = db.Column(db.Float, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    complete = db.Column(db.Boolean, default=False)
    complete_at = db.Column(db.DateTime)
    notes = db.Column(db.String)
    items = db.relationship('Item', backref='collection')

    def __init__(self, postcode, user_id):
        self.postcode = postcode
        self.user_id = user_id
        self.created_at = datetime.datetime.now()

    def set_complete(self, completed_at):
        """Marks a collection as complete"""
        self.complete = True
        self.complete_at = completed_at


class Item(db.Model):
    __tablename__='items'
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    itemtype_id = db.Column(db.Integer, db.ForeignKey('itemtypes.id'), nullable=False)
    name = db.Column(db.String(100), nullable=False)
    collection_id = db.Column(db.Integer, db.ForeignKey('collections.id'))
    donation_id = db.Column(db.Integer, db.ForeignKey('donations.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    def __init__(self, type_id, name):
        self.itemtype_id = type_id
        self.name = name


class ItemType(db.Model):
    __tablename__='itemtypes'
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    name = db.Column(db.String(100))
    items = db.relationship('Item', backref='itemtype')

    def __init__(self, name):
        self.name = name


class Donation(db.Model):
    __tablename__='donations'
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    date = db.Column(db.DateTime, nullable=False)
    hospital_id = db.Column(db.Integer, db.ForeignKey('hospitals.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    items = db.relationship('Item', backref='donation')

    def __init__(self, date, hospital_id, user_id):
        self.date = date
        self.hospital_id = hospital_id
        self.user_id = user_id


class Hospital(db.Model):
    __tablename__='hospitals'
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    location = db.Column(db.String(100), nullable=False)
    donations = db.relationship('Donation', backref='hospital')

    def __init__(self, name, location):
        self.name = name
        self.location = location
