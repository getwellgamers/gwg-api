# app/tests/test_item_model.py

from app import db
from app.api.models import Item, ItemType
from app.tests.base import BaseTestCase


class TestItemModel(BaseTestCase):
    def test_add_item(self):
        itemtype = ItemType('game')
        db.session.add(itemtype)
        db.session.commit()
        item = Item(itemtype.id, 'console')
        db.session.add(item)
        db.session.commit()
        self.assertTrue(item.id)
        self.assertEqual(item.name, 'console')
        self.assertEqual(item.itemtype_id, itemtype.id)
