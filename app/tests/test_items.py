# app/tests/test_items.py

import json

from app.tests.base import BaseTestCase
from app.tests.utils import add_donation, add_user, add_item


class TestItemService(BaseTestCase):
    """Test for the items service"""

    def test_add_item(self):
        """Ensure new items are added to the database"""
        pass

    def test_add_item_invalid_json(self):
        """Ensure an error is thrown when JSON object is empty"""
        pass

    def test_add_item_invalid_json_no_name(self):
        """Ensure an error is thrown when JSON object does not contain a name"""
        pass

    def test_add_item_invalid_json_no_itemtype(self):
        """Ensure an error is thrown when JSON object does not contain an itemtype id"""
        pass

    def test_add_item_invalid_json_no_user(self):
        """Ensure an error is thrown when JSON object does not contain a user id"""
        pass

    def test_single_item(self):
        """Ensure getting a single item behaves as expected"""
        pass

    def test_single_item_no_id(self):
        """Ensure an error is thrown when no id is provided"""
        pass

    def test_single_item_incorrect_id(self):
        """Ensure an error is thrown when an incorrect id is provided"""
        pass

    def test_all_items(self):
        """Ensure getting all items works as intended"""
        pass
