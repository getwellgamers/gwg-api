# app/tests/test_collections.py

import json

from app.tests.base import BaseTestCase
from app.tests.utils import add_collection, add_user


class TestCollectionService(BaseTestCase):
    """Test for the collections service"""

    def test_add_collection(self):
        """Ensure new collections are added to the database"""
        user = add_user('test', 'test@test.com', 'test', 'test')
        with self.client:
            response = self.client.post(
                '/collections',
                data=json.dumps(dict(
                    postcode='RM30BG',
                    user_id=user.id,
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertIn('Collection added', data['message'])
            self.assertIn('success', data['status'])

    def test_add_collection_invalid_json(self):
        """Ensure an error is thrown when JSON object is empty"""
        with self.client:
            response = self.client.post(
                '/collections',
                data=json.dumps(dict()),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_collection_invalid_json_no_postcode(self):
        """Ensure an error is thrown when JSON object does not contain a postcode"""
        user = add_user('test', 'test@test.com', 'test', 'test')
        with self.client:
            response = self.client.post(
                '/collections',
                data=json.dumps(dict(
                    user_id=user.id
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_collection_invalid_json_no_user(self):
        """Ensure an error is thrown when JSON object does not contain a user id"""
        with self.client:
            response = self.client.post(
                '/collections',
                data=json.dumps(dict(
                    postcode='RM30BG'
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_collection(self):
        """Ensure getting a single collection behaves as expected"""
        user = add_user('test', 'test@test.com', 'test', 'test')
        collection = add_collection('RM3 0BG', user.id)
        with self.client:
            response = self.client.get('/collections/' + str(collection.id))
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn('RM3 0BG', data['data']['postode'])
            self.assertEqual(user.id, data['data']['user_id'])

    def test_single_collection_no_id(self):
        """Ensure an error is thrown when no id is provided"""
        with self.client:
            response = self.client.get('/collections/blah')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Collection does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_collection_incorrect_id(self):
        """Ensure an error is thrown when an incorrect id is provided"""
        with self.client:
            response = self.client.get('/collections/999')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Collection does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_all_collections(self):
        """Ensure getting all collections works as intended"""
        user_1 = add_user('test', 'test@test.com', 'test', 'test')
        user_2 = add_user('lest', 'lest@lest.com', 'lest', 'lest')
        collection_1 = add_collection('RM3 0BG', user_1.id)
        collection_2 = add_collection('RM4 0BG', user_2.id)
        with self.client:
            response = self.client.get('/collections')
            data = json.loads(response.status_code.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn('success', data['status'])
            self.assertIn('RM3 0BG', data['data']['collections'][0]['postcode'])
            self.assertIn('RM3 0BG', data['data']['collections'][1]['postcode'])
            self.assertEqual(user_1.id, data['data']['collections'][0]['user_id'])
            self.assertEqual(user_2.id, data['data']['collections'][1]['user_id'])
