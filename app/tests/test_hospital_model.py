# app/tests/test_hospital_mdoel.py

from app import db
from app.api.models import Hospital
from app.tests.base import BaseTestCase


class TestHospitalModel(BaseTestCase):
    def test_add_hospital(self):
        hospital = Hospital('hospital', 'location')
        db.session.add(hospital)
        db.session.commit()
        self.assertTrue(hospital.id)
        self.assertEqual(hospital.name, 'hospital')
        self.assertEqual(hospital.location, 'location')