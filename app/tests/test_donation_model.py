# app/tests/test_donation_model.py

import datetime
from app import db
from app.api.models import Donation, Hospital, User
from app.tests.base import BaseTestCase


class TestDonationModel(BaseTestCase):
    def test_add_donation(self):
        user = User('tim', 'tim@tim.com', 'test', 'location')
        db.session.add(user)
        db.session.commit()
        hospital = Hospital('Quens Hospital', 'Romford')
        db.session.add(hospital)
        db.session.commit()
        donation = Donation(datetime.datetime.now(), hospital.id, user.id)
        db.session.add(donation)
        db.session.commit()
        self.assertTrue(donation.id)
        self.assertTrue(donation.date)
        self.assertEqual(donation.hospital_id, hospital.id)
        self.assertEqual(donation.user_id, user.id)
