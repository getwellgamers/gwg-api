# app/tests/test_users.py

import json

from app import db
from app.api.models import User
from app.tests.base import BaseTestCase
from app.tests.utils import add_user


class TestUserService(BaseTestCase):
    """Tests for the User Service"""

    def test_users(self):
        """Healthcheck with ping"""
        response = self.client.get('/ping')
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('pong', data['message'])
        self.assertIn('success', data['status'])

    def test_add_user(self):
        """Ensure new users can be added to the database"""
        with self.client:
            response = self.client.post(
                '/users',
                data=json.dumps(dict(
                    username='tim',
                    email='tim.thompson147@gmail.com',
                    password='test',
                    location='test'
                )),
                content_type='application/json',
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertIn('tim.thompson147@gmail.com', data['message'])
            self.assertIn('success', data['status'])

    def test_add_user_invalid_json(self):
        """Ensure error is thrown if the JSON object is empty"""
        with self.client:
            response = self.client.post(
                '/users',
                data=json.dumps(dict()),
                content_type='application/json',
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_user_invalid_json_keys_no_username(self):
        """Ensure error is thrown if JSON object does not have a username key"""
        with self.client:
            response = self.client.post(
                '/users',
                data=json.dumps(dict(
                    email='tim.thompson147@gmail.com',
                    password='test',
                    location='test'
                )),
                content_type='application/json',
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_user_invalid_json_keys_no_email(self):
        """Ensure error is thrown in JSON object does not have an email key"""
        with self.client:
            response = self.client.post(
                '/users',
                data=json.dumps(dict(
                    username='tim',
                    password='test',
                    location='test'
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_user_invalid_json_keys_no_password(self):
        """Ensure error is thrown if JSON object does not have a password key"""
        with self.client:
            response = self.client.post(
                '/users',
                data=json.dumps(dict(
                    username='tim',
                    email='test@test.com',
                    location='test'
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_user_invalid_json_keys_no_location(self):
        """Ensure error is thrown if JSON object does not have a location key"""
        with self.client:
            response = self.client.post(
                '/users',
                data=json.dumps(dict(
                    username='tim',
                    email='test@test.com',
                    password='test'
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_user_duplicate_email(self):
        """Ensure error is thrown if email is already in use"""
        with self.client:
            self.client.post(
                '/users',
                data=json.dumps(dict(
                    username='tim',
                    email='tim.thompson147@gmail.com',
                    password='test',
                    location='test'
                )),
                content_type='application/json',
            )
            response = self.client.post(
                '/users',
                data=json.dumps(dict(
                    username='tim',
                    email='tim.thompson147@gmail.com',
                    password='tim',
                    location='test'
                )),
                content_type='application/json',
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Email already in use', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_user(self):
        """Ensure getting a single user behaves as expected"""
        user = add_user('tim', 'tim.thompson147@gmail.com', 'test', 'test')
        with self.client:
            response = self.client.get('/users/' + str(user.id))
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn('tim', data['data']['username'])
            self.assertIn('tim.thompson147@gmail.com', data['data']['email'])
            self.assertIn('test', data['data']['location'])
            self.assertIn('success', data['status'])

    def test_single_user_no_id(self):
        """Ensure error is thrown if no user id is provided"""
        with self.client:
            response = self.client.get('/users/blah')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('User does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_user_incorrect_id(self):
        """Ensure error is thrown if the provided user id does not exist"""
        with self.client:
            response = self.client.get('/users/999')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('User does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_all_users(self):
        """Ensure all users are returned successfully"""
        with self.client:
            add_user('tim', 'tim.thompson147@gmail.com', 'test', 'tim_town')
            add_user('jim', 'jim@bob.com', 'test', 'jim_town')
            add_user('bob', 'bob@jim.com', 'test', 'bob_town')
            response = self.client.get('/users')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(data['data']['users']), 3)
            self.assertIn('tim', data['data']['users'][0]['username'])
            self.assertIn('jim', data['data']['users'][1]['username'])
            self.assertIn('bob', data['data']['users'][2]['username'])
            self.assertIn('tim.thompson147@gmail.com', data['data']['users'][0]['email'])
            self.assertIn('jim@bob.com', data['data']['users'][1]['email'])
            self.assertIn('bob@jim.com', data['data']['users'][2]['email'])
            self.assertIn('tim_town', data['data']['users'][0]['location'])
            self.assertIn('jim_town', data['data']['users'][1]['location'])
            self.assertIn('bob_town', data['data']['users'][2]['location'])
            self.assertIn('success', data['status'])