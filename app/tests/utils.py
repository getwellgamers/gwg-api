# app/tests/utils.py

from app import db
from app.api.models import User, Hospital, ItemType, Donation, Item, Collection


def add_user(username, email, password, location):
    user = User(username, email, password, location)
    db.session.add(user)
    db.session.commit()
    return user


def add_hospital(name, location):
    hospital = Hospital(name, location)
    db.session.add(hospital)
    db.session.commit()
    return hospital


def add_itemtype(name):
    itemtype = ItemType(name)
    db.session.add(itemtype)
    db.session.commit()
    return itemtype


def add_donation(date, hospital_id, user_id):
    donation = Donation(date, hospital_id, user_id)
    db.session.add(donation)
    db.session.commit()
    return donation


def add_item(type_id, name):
    item = Item(type_id, name)
    db.session.add(item)
    db.session.commit()
    return item


def add_collection(postcode, user_id):
    collection = Collection(postcode, user_id)
    db.session.add(collection)
    db.session.commit()
    return collection
