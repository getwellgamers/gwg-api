# app/tests/test_donations.py

import json, datetime

from app.tests.base import BaseTestCase
from app.tests.utils import add_donation, add_user, add_hospital


class TestDonationServce(BaseTestCase):
    """Test for the donations service"""

    def test_add_donation(self):
        """Ensure new donations are added to the database"""
        user = add_user('tim', 'tim@tim.com', 'tim', 'London')
        hospital = add_hospital('queens', 'Romford')
        with self.client:
            response = self.client.post(
                '/donations',
                data=json.dumps(dict(
                    date='15/01/2017',
                    hospital_id=hospital.id,
                    user_id=user.id
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertIn('Donation added', data['message'])
            self.assertIn('success', data['status'])

    def test_add_donation_invalid_json(self):
        """Ensure an error is thrown when json object is empty"""
        with self.client:
            response = self.client.post(
                '/donations',
                data=json.dumps(dict()),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_donation_invalid_json_no_date(self):
        """Ensure an error is thrown if json object does not contain a date"""
        user = add_user('tim', 'tim@tim.com', 'tim', 'London')
        hospital = add_hospital('queens', 'Romford')
        with self.client:
            response = self.client.post(
                '/donations',
                data=json.dumps(dict(
                    hospital_id=hospital.id,
                    user_id=user.id
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_donation_invalid_json_no_hospital(self):
        """Ensure an error is thrown if json object does not contain a hospital id"""
        user = add_user('tim', 'tim@tim.com', 'tim', 'London')
        with self.client:
            response = self.client.post(
                '/donations',
                data=json.dumps(dict(
                    date='13/12/2017',
                    user_id=user.id
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_donation_invalid_json_no_user(self):
        """Ensure an error is thrown if json object does not contain a user id"""
        hospital = add_hospital('queens', 'Romford')
        with self.client:
            response = self.client.post(
                '/donations',
                data=json.dumps(dict(
                    date='13/12/2017',
                    hospital_id=hospital.id
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_donation(self):
        """Ensure that getting a single donation works as expected"""
        user = add_user('tim', 'tim@tim.com', 'tim', 'London')
        hospital = add_hospital('queens', 'Romford')
        date_now = datetime.datetime.now()
        donation = add_donation(date_now, hospital.id, user.id)
        with self.client:
            response = self.client.get('/donations/' + str(donation.id))
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn(str(date_now), data['data']['date'])
            self.assertEqual(hospital.id, data['data']['hospital_id'])
            self.assertEqual(user.id, data['data']['user_id'])

    def test_single_donation_no_id(self):
        """Ensure an error is thrown when getting a donation with no id"""
        with self.client:
            response = self.client.get('/donations/blah')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Donation does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_donation_incorrect_id(self):
        """Ensure an error is thrown when getting a donation with an incorrect id"""
        with self.client:
            response = self.client.get('/donations/999')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Donation does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_all_donations(self):
        """Ensure getting all donations works as expected"""
        user_1 = add_user('tim', 'tim@tim.com', 'tim', 'London')
        hospital_1 = add_hospital('queens', 'Romford')
        user_2 = add_user('tom', 'tom@tom.com', 'test', 'tom')
        hospital_2 = add_hospital('kings', 'brentwood')
        date_now = datetime.datetime.now()
        donation_1 = add_donation(date_now, hospital_1.id, user_1.id)
        donation_2 = add_donation(date_now, hospital_2.id, user_2.id)
        with self.client:
            response = self.client.get('/donations')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn(str(date_now), data['data']['donations'][0]['date'])
            self.assertEqual(hospital_1.id, data['data']['donations'][0]['hospital_id'])
            self.assertEqual(user_1.id, data['data']['donations'][0]['user_id'])

            self.assertIn(str(date_now), data['data']['donations'][1]['date'])
            self.assertEqual(hospital_2.id, data['data']['donations'][1]['hospital_id'])
            self.assertEqual(user_2.id, data['data']['donations'][1]['user_id'])

            self.assertIn('success', data['status'])
