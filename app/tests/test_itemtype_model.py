# app/tests/test_itemtype_model.py

from app import db
from app.api.models import ItemType
from app.tests.base import BaseTestCase


class TestItemTypeModel(BaseTestCase):
    def test_add_itemtype(self):
        item_type = ItemType('game')
        db.session.add(item_type)
        db.session.commit()
        self.assertTrue(item_type.id)
        self.assertEqual(item_type.name, 'game')
