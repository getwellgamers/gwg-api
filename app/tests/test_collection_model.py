# app/tests/test_collection_model.py

from app import db
from app.api.models import Collection
from app.tests.base import BaseTestCase


class TestCollectionModel(BaseTestCase):
    def test_add_collection(self):
        collection = Collection('RM3 0BG', 33.33, 33.33)
        db.session.add(collection)
        db.session.commit()
        self.assertTrue(collection.id)
        self.assertTrue(collection.created_at)
        self.assertEqual(collection.postcode, 'RM3 0BG')
        self.assertEqual(collection.lng, 33.33)
        self.assertEqual(collection.lat, 33.33)
        self.assertFalse(collection.complete)
