# app/tests/test_hospitals.py

import json

from app.tests.base import BaseTestCase
from app.tests.utils import add_hospital


class TestHospitalService(BaseTestCase):
    """Tests for the hospital service"""

    def test_add_hospital(self):
        """Ensure new hospitals can be added to the database"""
        with self.client:
            response = self.client.post(
                '/hospitals',
                data=json.dumps(dict(
                    name='test',
                    location='location'
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertIn('test', data['message'])
            self.assertIn('success', data['status'])

    def test_add_hospital_invalid_json(self):
        """Ensure error is thrown if the JSON object is empty"""
        with self.client:
            response = self.client.post(
                '/hospitals',
                data=json.dumps(dict()),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_hospital_invalid_json_no_name(self):
        """Ensure error is thrown if the JSON object has no name key"""
        with self.client:
            response = self.client.post(
                '/hospitals',
                data=json.dumps(dict(
                    location='location'
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_hospital_invalid_json_no_location(self):
        """Ensure error is thrown if the JSON object has no location key"""
        with self.client:
            response = self.client.post(
                '/hospitals',
                data=json.dumps(dict(
                    name='name'
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_hospital_duplicate(self):
        """Ensure error is thrown if this hospital already exists"""
        hospital = add_hospital('justatest', 'location')
        with self.client:
            response = self.client.post(
                '/hospitals',
                data=json.dumps(dict(
                    name='justatest',
                    location='location'
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Hospital already exists', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_hospital(self):
        """Ensure getting a single hospital behaves as expected"""
        hospital = add_hospital('name', 'location')
        with self.client:
            response = self.client.get('/hospitals/' + str(hospital.id))
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn('name', data['data']['name'])
            self.assertIn('location', data['data']['location'])
            self.assertIn('success', data['status'])

    def test_single_hospital_no_id(self):
        """Ensure error is thrown if no hospital id is given"""
        hospital = add_hospital('name', 'location')
        with self.client:
            response = self.client.get('/hospitals/blah')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Hospital does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_hospital_incorrect_id(self):
        """Ensure error is thrown if hospital id does not exist"""
        hospital = add_hospital('name', 'location')
        with self.client:
            response = self.client.get('/hospitals/999')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Hospital does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_all_hospitals(self):
        """Ensure all hospitals are returned successfully"""
        add_hospital('hospital1', 'location1')
        add_hospital('hospital2', 'location2')
        add_hospital('hospital3', 'location3')
        with self.client:
            response = self.client.get('/hospitals')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(data['data']['hospitals']), 3)
            self.assertIn('hospital1', data['data']['hospitals'][0]['name'])
            self.assertIn('hospital2', data['data']['hospitals'][1]['name'])
            self.assertIn('hospital3', data['data']['hospitals'][2]['name'])
            self.assertIn('location1', data['data']['hospitals'][0]['location'])
            self.assertIn('location2', data['data']['hospitals'][1]['location'])
            self.assertIn('location3', data['data']['hospitals'][2]['location'])
            self.assertIn('success', data['status'])
