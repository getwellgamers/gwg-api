# app/tests /test_itemtypes.py

import json

from app.tests.base import BaseTestCase
from app.tests.utils import add_itemtype


class TestItemTypeService(BaseTestCase):
    """Tests for the itemtypes service"""

    def test_add_itemtype(self):
        """Ensure new itemtypes can be added to the database"""
        with self.client:
            response = self.client.post(
                '/itemtypes',
                data=json.dumps(dict(
                    name='name'
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertIn('name', data['message'])
            self.assertIn('success', data['status'])

    def test_add_itemtype_invalid_json(self):
        """Ensure error is thrown if json object is empty"""
        with self.client:
            response = self.client.post(
                '/itemtypes',
                data=json.dumps(dict(
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_itemtype_duplicate(self):
        """Ensure error is thrown if itemtype already exists"""
        add_itemtype('Game')
        with self.client:
            response = self.client.post(
                '/itemtypes',
                data=json.dumps(dict(
                    name='Game'
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Itemtype already exists', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_itemtype(self):
        """Ensure getting a single itemtype works as expected"""
        itemtype = add_itemtype('Game')
        with self.client:
            response = self.client.get('/itemtypes/' + str(itemtype.id))
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn('Game', data['data']['name'])
            self.assertIn('success', data['status'])

    def test_single_itemtype_no_id(self):
        """Ensure an error is thrown if no itemtype id is given"""
        itemtype = add_itemtype('Game')
        with self.client:
            response = self.client.get('/itemtypes/blah')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Itemtype does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_itemtype_incorrect_id(self):
        """Ensure an error is thrown if itemtype id does not exist"""
        itemtype = add_itemtype('Game')
        with self.client:
            response = self.client.get('/itemtypes/999')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Itemtype does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_all_itemtypes(self):
        """Ensure getting all itemtypes works as expected"""
        add_itemtype('Game')
        add_itemtype('Console')
        with self.client:
            response = self.client.get('/itemtypes')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn('Game', data['data']['itemtypes'][0]['name'])
            self.assertIn('Console', data['data']['itemtypes'][1]['name'])