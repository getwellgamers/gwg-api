# manage.py

import unittest, coverage
from flask_script import Manager
from app import create_app, db
from app.api.models import User
from flask_migrate import MigrateCommand

COV = coverage.coverage(
    branch=True,
    include='app/*',
    omit=[
        'app/tests/*'
    ]
)
COV.start()

app = create_app()
manager = Manager(app)

manager.add_command('db', MigrateCommand)


@manager.command
def recreate_db():
    """Recreates a database"""
    db.drop_all()
    db.create_all()
    db.session.commit()


@manager.command
def seed_db():
    """Seeds database with sample data"""
    db.session.add(User(username='jim', email='jim@bob.com', password='test', location='town'))
    db.session.add(User(username='bob', email='bob@jim.com', password='test', location='town'))
    db.session.commit()


@manager.command
def test():
    """Runs the tests without code coverage"""
    tests = unittest.TestLoader().discover('app/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


@manager.command
def cov():
    """Runs the tests with code coverage"""
    tests = unittest.TestLoader().discover('app/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        COV.stop()
        COV.save()
        print ('Coveragetest Summary:')
        COV.report()
        COV.html_report()
        COV.erase()
        return 0
    return 1

if __name__ == '__main__':
    manager.run()
